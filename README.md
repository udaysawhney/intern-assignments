# Assignment - Server Onboarding

## Project

```
This project aims at determining the understanding gained with Java fundamentals and appropriate frameworks built for java at enterprise scale.
```

### Business Context

```
We will be primarily dealing with 5 entities.
-> Tenant(Issuer), Business, Store, Counter and Terminal
Every resource resides under a root resource -> tenant / issuer / financial institution. the identifier being a tenantId or issuer.
Every tenant can onboard N businesses / merchants.
Each merchant can have N stores.
Each store can hold multiple counters.
A counter can support N terminals for payments.
```
Reference for data modelling : [Link](http://atlasdocs0zetaapps.internal.olympus-world.zeta.in/docs/merchant-management/merchant-data-modeling/)


1. Create a web server using the spring
   framework. [Spring Docs](https://docs.spring.io/spring-framework/docs/current/reference/html/)
    1. Provide an HTTP server implementation using `Spring web / webflux`.
2. Define Entities as Java `POJOs`.

```java
class Business {
    // define necessary variables to best identity and describe an entity
    private final Long tenantId;
    private final Long businessId;
    private final String businessName;
    private final Map<String, String> attributes;
}

class Store {
}

class Counter {
}

class Terminal {
}

class User {
}

class Role {
}
```

3. Configure a working instance of data source `PostgresSQL`.
    * Define the necessary tables and their relations.
4. Expose and implement necessary logical constructs of `Controller`, `Service`, `Model`, `Repository` as packages
   or `Maven Modules`.
5. Implement CRUD endpoints for as many resources(e.g business, store, counter, terminal) as possible, performance and abuse of contract needs to be handled with utmost priority.
6. Usage of any helpful dependencies like `lombok`, `guava` is much encouraged :smiley:
7. Define ***2*** `application.properties` resource files to represent environments `common-zone` , `beta-zone`.
8. Use `Junit5` for writing testcases, both unit and integration tests.
9. Observability
    1. Any implementation for logging `Log4j2, logback` can be used.
    2. Expose system metrics via `micrometer` and `spring-actuator` .
    3. Try to make a `heap / thread dump` and analyze on any online tool.
10. Api Documentation using `Swagger/OpenAPI v3`.
11. SonarQube for static code analysis

***Optional [Additional Service]***

- Create a cron management service, which onboards any entity/resource of choice (e.g business, store, counter,
  terminal) using the format (csv/ json), this service internally calls the first service on its exposed create
  endpoints for the resource.

### Acceptance Criteria

| Scope                    | Expectation                                                                                 | 
|--------------------------|---------------------------------------------------------------------------------------------|
| Entity Modelling         | Efficiently modelling entities -> business, store , counter, terminal                       |
| Database  Modelling      | Correctly `modelling tables and relations` (normalization) and separation of concern.       |
| Asynchronous Tools       | Correct use of `CompletionStage interface` / Project Reactor                                |
| Project / Code Structure | Maintenance of packages / modules  and `separation of concern`.                             |
| Testing                  | Test coverage of the entire project has a **minimum threshold** of **75%**.                 |
| Observability            | `Efficient logging` is a must, `visualization of metrics` needs to taken care of `Grafana`. |
| Documentation            | The documentation using any tool should hold enough context for any black box testing.      |


### Additions:

1. Auto generate ID as UUID
2. Introduce tenant as an entity with validation
3. Error handling
4. Update and soft delete API for business and store
5. Filter in the get APIs by name